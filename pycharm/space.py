def power2(x):
    if x is None:
        return None
    return x * x
